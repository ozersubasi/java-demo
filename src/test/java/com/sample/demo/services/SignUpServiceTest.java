package com.sample.demo.services;

import com.sample.demo.models.SignUpRequest;
import com.sample.demo.models.SignUpResponse;
import com.sample.demo.repository.SignUpRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verifyZeroInteractions;


@RunWith(MockitoJUnitRunner.class)
public class SignUpServiceTest {
    @InjectMocks
    private SignUpService signUpService;

    @Mock
    private SignUpRepository signUpRepository;

    @Test
    public void it_should_get_sign_up() {
        //given
        SignUpRequest signUpRequest = new SignUpRequest();
        SignUpResponse signUpResponse = new SignUpResponse();
        given(signUpRepository.CreateNewUser(any(SignUpRequest.class))).willReturn(signUpResponse);

        //when
        SignUpResponse result = signUpService.signUp(signUpRequest);

        //then
        assertThat(result).isEqualTo(signUpResponse);
    }
    @Test
    public void check_if_it_is_email() {
        //given
        SignUpRequest signUpRequest= new SignUpRequest();
        signUpRequest.setEmail("testtest.com");
        //when
        SignUpResponse result= signUpService.signUp(signUpRequest);
        //then
        verifyZeroInteractions(signUpRepository);
        assertThat(result).isNull();

    }
}