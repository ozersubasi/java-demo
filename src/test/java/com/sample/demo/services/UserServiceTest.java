package com.sample.demo.services;

import com.sample.demo.models.UserRequest;
import com.sample.demo.models.UserResponse;
import com.sample.demo.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Test
    public void it_should_get_login() {
        //given
        UserRequest userRequest = new UserRequest();
        UserResponse userResponse = new UserResponse();
        userRequest.setEmail("email");
        userRequest.setPassword("password");
        given(userRepository.findByEmailAndPassword(any(UserRequest.class))).willReturn(userResponse);
        //when
        UserResponse response= userService.getLogin(userRequest);

        //then
        assertThat(response).isEqualTo(userResponse);
    }

    @Test
    public void it_should_return_null_when_email_or_password_is_null() {
        //given
        UserRequest userRequest = new UserRequest();
        userRequest.setEmail(null);
        userRequest.setPassword(null);

        //when
        UserResponse response = userService.getLogin(userRequest);

        //then
        verifyZeroInteractions(userRepository);
        assertThat(response).isNull();
    }

    @Test
    public void it_should_return_null_when_password_is_over_16digits() {

        //given
        UserRequest userRequest = new UserRequest();
        userRequest.setEmail("email");
        userRequest.setPassword("123456789asdfghjk");

        //when
        UserResponse response= userService.getLogin(userRequest);

        //then
        verifyZeroInteractions(userRepository);
        assertThat(response).isNull();
    }

}