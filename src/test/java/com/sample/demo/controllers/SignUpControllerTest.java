package com.sample.demo.controllers;

import com.sample.demo.models.SignUpRequest;
import com.sample.demo.models.SignUpResponse;
import com.sample.demo.services.SignUpService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.http.MediaType;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = SignUpController.class)
public class SignUpControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SignUpService signUpService;

    @Test
    public void it_should_sign_up() throws Exception {
        //given
        SignUpResponse signUpResponse=new SignUpResponse();
        given(signUpService.signUp(any(SignUpRequest.class))).willReturn(signUpResponse);

        //when
        ResultActions resultActions=mockMvc.perform(post("/signup")
        .contentType(MediaType.APPLICATION_JSON).content("{\n" +
                        " \"email\": \"test@test.com\",\n" +
                        " \"password\": \"123456\",\n" +
                        " \"name\": \"testname\",\n" +
                        " \"birthday\": \"01.01.1995\",\n" +
                        " \"username\": \"testuser\"\n" +
                        "}"));
        //then
        resultActions.andExpect(status().isOk());

        ArgumentCaptor<SignUpRequest> requestCaptor= ArgumentCaptor.forClass(SignUpRequest.class);
        verify(signUpService).signUp(requestCaptor.capture());
        SignUpRequest signUpRequest= requestCaptor.getValue();
        assertThat(signUpRequest.getEmail()).isEqualTo("test@test.com");
    }

}
