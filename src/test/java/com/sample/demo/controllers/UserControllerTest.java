package com.sample.demo.controllers;

import com.sample.demo.models.UserRequest;
import com.sample.demo.models.UserResponse;
import com.sample.demo.services.UserService;
import com.sun.xml.internal.ws.api.pipe.ContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.http.MediaType;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    public void it_should_get_login() throws Exception {
        //given
        UserResponse userResponse= new UserResponse();

        given(userService.getLogin(any(UserRequest.class))).willReturn(userResponse);

        //when
        ResultActions resultActions= mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON).content("{\n" +
                        "  \"email\": \"test@test.com\",\n" +
                        "  \"password\": \"123456\"\n" +
                        "}"));

        //then
        resultActions.andExpect(status().isOk());

        ArgumentCaptor<UserRequest> requestCaptor= ArgumentCaptor.forClass(UserRequest.class);
        verify(userService).getLogin(requestCaptor.capture());
        UserRequest userRequest= requestCaptor.getValue();
        assertThat(userRequest.getEmail()).isEqualTo("test@test.com");
        assertThat(userRequest.getPassword()).isEqualTo("123456");

    }

}