package com.sample.demo.models;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class SignUpRequest {

    private String email;
    private String password;
    private String name;
    private Date birthday;
    private String username;

}
