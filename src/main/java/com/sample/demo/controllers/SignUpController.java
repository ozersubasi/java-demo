package com.sample.demo.controllers;

import com.sample.demo.models.SignUpRequest;
import com.sample.demo.models.SignUpResponse;
import com.sample.demo.services.SignUpService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/signup")
public class SignUpController {

    private final SignUpService signUpService;


    public SignUpController(SignUpService signUpService) {
        this.signUpService = signUpService;
    }

    @PostMapping
    public SignUpResponse signUp(@RequestBody  SignUpRequest signUpRequest) {
        return signUpService.signUp(signUpRequest);
    }
}
