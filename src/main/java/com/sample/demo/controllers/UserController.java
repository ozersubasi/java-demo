package com.sample.demo.controllers;

import com.sample.demo.models.UserRequest;
import com.sample.demo.models.UserResponse;
import com.sample.demo.services.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public UserResponse getLogin(@RequestBody UserRequest userRequest){
        return userService.getLogin(userRequest);
    }
}
