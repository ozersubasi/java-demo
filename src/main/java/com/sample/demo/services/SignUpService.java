package com.sample.demo.services;

import com.sample.demo.models.SignUpRequest;
import com.sample.demo.models.SignUpResponse;
import com.sample.demo.repository.SignUpRepository;
import org.springframework.stereotype.Service;

@Service
public class SignUpService {
    private SignUpRepository signUpRepository;

    public SignUpService(SignUpRepository signUpRepository) {
        this.signUpRepository = signUpRepository;
    }

    public SignUpResponse signUp(SignUpRequest signUpRequest) {
        if(!signUpRequest.getEmail().contains("@")) {
            return null;
        }
        return signUpRepository.CreateNewUser(signUpRequest);
    }
}
