package com.sample.demo.services;

import com.sample.demo.models.UserRequest;
import com.sample.demo.models.UserResponse;
import com.sample.demo.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private  final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserResponse getLogin(UserRequest userRequest) {
        if(userRequest.getEmail() == null || userRequest.getPassword() == null) {
            return null;
        }

        if(userRequest.getPassword().length()>16) {
            return null;
        }

        return userRepository.findByEmailAndPassword(userRequest);

    }
}
